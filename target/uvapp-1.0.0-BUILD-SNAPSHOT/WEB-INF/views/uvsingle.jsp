
<%@ page session="false" %>

<%@ include file="/WEB-INF/views/layout.jsp" %> 


<!-- PUT THIS IN THE CSS STYLE FILE -->
<style>
.container {
  width: 80%;
  margin: 15px auto;
}
</style>


<html>

<head>
	<script src="<c:url value="/resources/assets/js/Chart.js" />"></script>
	<title>Current UV</title>
	
</head>


<body>

  
<div class="container">
  <div class="pure-g" style="text-align:center">
  	<div class="pure-u-1 pure-u-md-1-2">
  	  	<h2>Current UV Rating</h2>
  		<P> The current UV is ${model.uvSingle} Joules per Square Meter. </P>

  	
  	</div>
  	
  	<div class="pure-u-1 pure-u-md-1-2">

	 <form name='f' action="uvsingle" method='POST'>
	                Number of hours in the sun:<br>
	 		<input type="text" name="hours" value="1"/><br><br>
			<button class="pure-button pure-button-primary" name="submit" type="submit" value="Check In!">Check In!</button>
	  </form>
  	</div>
  
  </div>
  		${model.Footer}
  <div>
    <canvas id="uvChart"></canvas>
  </div>
</div>

<h1>
	<fmt:message key="heading"/>
</h1>





<!--  HERE BE JAVASCRIPT -->
<script>
var ctx = document.getElementById('uvChart').getContext('2d');
var uvHistogram =<c:out value="${model.uvHistogram}"/>;
var myChart = new Chart(ctx, {
  type: 'line',
  data: {
    labels: ['12am', '1am', '2am', '3am', '4am', '5am', '6am', '7am', '8am', '9am', '10am', '11am', '12pm', '1pm', '2pm', '3pm', '4pm', '5pm', '6pm', '7pm', '8pm', '9pm', '10pm', '11pm'],
    datasets: [{
      label: 'Joules/Square Meter',
      data: uvHistogram,
      backgroundColor: "rgba(153,255,51,0.6)"
    }]
  }
});
</script>


</body>

</html>
