<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>
	<title>Home</title>
</head>
<body>

<%@ include file="/WEB-INF/views/layout.jsp" %> 

<div class="text-box">
	<h1>
		UV Information 
	</h1>
	
		<h2>
		 	What is Ultraviolet Radiation? 
		</h2>
	  <p> Ultraviolet (UV) radiation is one component radiation emitted by the sun alongside visible light and heat. Like with light, UV consists of three main regions: UVA, UVB, and UVC, with the atmosphere filtering out all UVC and most UVB. It is this exposure to UVA and the UVB that reaches the Earth's surface that leads to damage to human beings. That being said, UV exposure is essential to the body's development of Vitamin D, and hence a balance must be struck in order to live a healthy lifestyle. </p>
	
		<img class="uv-infographic" src='<c:url value="/resources/assets/img/UV-penetration-edited.png" />'/>
	
		<h2>
			UV Overexposure
		</h2>
		
		<p>UV radiation is a major contributor to the growth and development of melanoma, or skin cancer, which is a dangerous but preventable cancer. Unfortunately, despite its preventability, it is expected that two of three Australians will be develop it by the age of 70. Further, regular overexposure to UV radiation can result in significant short term damage to the skin, leading to sunburn, dryness and early aging, cataracts, and various other conditions. The best means to protect against UV overexposure is through remaining in the shade and ensuring that adequate protection is worn, including the wearing of hats, clothes and sunglasses, as well as usage of sunscreen. However, despite this being common knowledge, Australians are still unaware of the the level of damage they are doing to themselves, and as a result are often ill-prepared for the deleterious impact of UV radiation. </p>
		
		<h2>
			UV Underexposure
		</h2>
		<p>
			Vitamin D is an essential vitamin to human health that is only available in large quantities through synthesis by the human body via exposure to UV radiation. It is an essential component for bone and muscle development as well as mental health and without sufficient quantities, can lead to issues in these areas. Vitamin D deficiency is a significant issue, with an estimated 1 billion people worldwide suffering from some degree of deficiency. Unfortunately, alternatives to sun exposure, such as diet, are not adequate solutions to resolve this deficiency. Hence a careful balance must be struck between being protected from getting too much exposure to sunlight, and not getting enough exposure so as to be deficient in vitamin D. Groups that are most at risk of vitamin D deficiency include those who have naturally very dark skin, those without much sun exposure, and those with a disability. Hence, being able to measure both point-in-time and cumulative exposure would be invaluable in helping finding this right balance. 
		 </p>	
	
	 <p>${model.Footer}</p> 
</div>

</body>
</html>
