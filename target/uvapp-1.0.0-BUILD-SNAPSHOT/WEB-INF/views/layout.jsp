
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css"/>
<link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/grids-responsive-min.css"/>
<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css"/>
<link rel="stylesheet" href='<c:url value="/resources/assets/css/marketing.css" />'/>

<div class="header">
    <div class="home-menu pure-menu pure-menu-horizontal pure-menu-fixed">
    	<div class="pure-g">
    		<div class="pure-u-1 pure-u-md-1-3"> 
				<a class="pure-menu-heading" href="index">UV Monitor</a>
			</div>
    		<div class="pure-u-1 pure-u-md-1-3">
				<ul class="pure-menu-list">
            		${model.Sidebar}
        		</ul>			
			</div>
    		<div class="pure-u-1 pure-u-md-1-3">
    			<ul class="pure-menu-list">
					${model.Banner}			
    			</ul>
			</div>
		</div>

    </div>
    
</div>

<p style="text-align: center">${model.Header}</p>
${model.Popup}

