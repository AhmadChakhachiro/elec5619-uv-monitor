-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: uv_app
-- ------------------------------------------------------
-- Server version	5.7.16-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bomdata`
--

DROP TABLE IF EXISTS `bomdata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bomdata` (
  `location` varchar(30) NOT NULL,
  `latitude` float NOT NULL,
  `longitude` float NOT NULL,
  `uv` float NOT NULL,
  `uvtimestamp` datetime NOT NULL,
  PRIMARY KEY (`location`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bomdata`
--

LOCK TABLES `bomdata` WRITE;
/*!40000 ALTER TABLE `bomdata` DISABLE KEYS */;
INSERT INTO `bomdata` VALUES ('Sydney Airport',2,2,27,'2016-10-14 12:12:12'),('Sydney Observatory Hill',0,0,24,'2016-10-14 12:12:12');
/*!40000 ALTER TABLE `bomdata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `idProduct` int(11) NOT NULL AUTO_INCREMENT,
  `Description` varchar(45) NOT NULL,
  `Price` int(11) NOT NULL,
  PRIMARY KEY (`idProduct`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1,'Test Product',24),(3,'Test Add 2',40);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `riskdata`
--

DROP TABLE IF EXISTS `riskdata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `riskdata` (
  `email` varchar(30) NOT NULL,
  `riskValue` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`email`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `riskdata`
--

LOCK TABLES `riskdata` WRITE;
/*!40000 ALTER TABLE `riskdata` DISABLE KEYS */;
INSERT INTO `riskdata` VALUES ('alan@robertson.com',0),('archi2@test.com',13),('archi3@test.com',10),('archi@test.com',10),('king@kong.m',0),('matt@matt.com',0),('matt@mills.com',0),('qwop@qwop.com',0),('qwop@qwop.qwop',0),('test2@test.com',0),('test@test.com',0);
/*!40000 ALTER TABLE `riskdata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `email` varchar(30) NOT NULL,
  `password` varchar(60) NOT NULL,
  `doneQuest` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`email`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES ('alan@robertson.com','$2a$10$lz5Xg8mVWcnGEL1YjnhMW./2rvumCJGBRo1uZBhUfYCzNcMOHMkWm',1),('archi2@test.com','$2a$10$s7Ir9XlurGFA3Icm4RZzxeexsPOWxAg.0eLaLckLZyhZ9bh1JycCK',1),('archi3@test.com','$2a$10$mq854PfGBvPc0C1eBs79JO1iSfcXDY6dxg2yWT.osH4q3Af86XT.6',1),('archi@test.com','$2a$10$dLkJfYasJaLnatxppa.CquCThNrFS.UGpDIjmBYVOHo91C1i3QtQC',1),('king@kong.m','$2a$10$DNNH0JnrH/ooTrJjQxfgN.r.XpwBlIRN0AOb51cKI0lGXyzvifiHa',1),('matt@matt.com','$2a$10$tCUWEEThc0L1qii8D8F7PeTM3LQGToJEyHyk4mkyoxqFDfrW5FOJi',1),('matt@mills.com','$2a$10$6tCTwM3HDgdAy.oXeZjE/eTa/sYTPdn2c424lGzHlGubmJze/Im4O',1),('qwop@qwop.com','$2a$10$ryI5G1fgKHfAl4WxqjaV3uQHiK7vMt2doa/rAi9B20/jjsw2GQ6Ty',1),('qwop@qwop.qwop','$2a$10$x0D0CrXBj.fXfxcer.0AOOOPb.NV8sOAgLj9f0N3SX4IiQ3U6v6R.',1),('test2@test.com','$2a$10$7Zzjftr0j0mMQ8RL6TtaTO6Y6XUt9ieXUgCbo8d9xGN4kUJ86Cak.',1),('test@test.com','$2a$10$UDI.6R2eEliAHAGGwWl4iuxfYb68l92VIvRlkc/Ed.qosand/O6Ha',1);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userdata`
--

DROP TABLE IF EXISTS `userdata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userdata` (
  `dataId` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(30) NOT NULL,
  `uvTimestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `uv` float NOT NULL,
  `hours` double DEFAULT NULL,
  PRIMARY KEY (`dataId`),
  KEY `userdata_ibfk_1_idx` (`email`),
  CONSTRAINT `userdata_ibfk_1` FOREIGN KEY (`email`) REFERENCES `user` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userdata`
--

LOCK TABLES `userdata` WRITE;
/*!40000 ALTER TABLE `userdata` DISABLE KEYS */;
INSERT INTO `userdata` VALUES (4,'matt@matt.com','2016-10-18 08:27:22',0.749161,NULL),(12,'alan@robertson.com','2016-09-21 05:15:15',16.2013,12),(13,'alan@robertson.com','2016-09-21 05:15:15',15.6654,100),(14,'alan@robertson.com','2016-09-21 05:15:15',15.3453,1),(15,'alan@robertson.com','2016-09-21 05:15:15',15.3453,100),(16,'alan@robertson.com','2016-09-21 05:15:15',15.3453,122),(17,'alan@robertson.com','2016-09-21 05:15:15',13.8719,12),(18,'archi@test.com','2016-10-23 04:27:16',13.4589,1),(20,'archi@test.com','2016-10-24 11:23:53',0,0);
/*!40000 ALTER TABLE `userdata` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-10-25 14:33:57
