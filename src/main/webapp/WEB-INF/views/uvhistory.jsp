<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>

<%@ include file="/WEB-INF/views/layout.jsp" %> 


<!-- PUT THIS IN THE CSS STYLE FILE -->
<style>
.container {
  width: 80%;
  margin: 15px auto;
}
</style>


<html>

<head>
	<script src="<c:url value="/resources/assets/js/Chart.js" />"></script>
	<title>Current UV</title>
</head>
<body>


<div class="container">

  <div class="pure-g" style="text-align:center">
  	<div class="pure-u-1 pure-u-md-1-2">
  	  	<h2>Current UV Rating</h2>
  		<P> The current UV is ${model.uvSingle} Joules per Square Meter. </P>

  	
  	</div>
  	
  	<div class="pure-u-1 pure-u-md-1-2">

	 <form name='f' action="uvsingle" method='POST'>
	                Number of hours in the sun:<br>
	 		<input type="text" name="hours" value="1"/><br><br>
			<button class="pure-button pure-button-primary" name="submit" type="submit" value="Check In!">Check In!</button>
	  </form>
  	</div>
  
  </div>
  <div>
    <canvas id="uvChart"></canvas>
  </div>
</div>

<h1>
	<fmt:message key="heading"/>
</h1>


${model.Footer}


<!--  HERE BE JAVASCRIPT -->
<script>
var ctx = document.getElementById('uvChart').getContext('2d');
var uvtimeData =<c:out value="${model.timedata}"/>;

var uvDateTime = [];

for(var i = 0; i < uvtimeData.length; i++) {
	var dateTime = new Date(uvtimeData[i]);
	var dateString = dateTime.toLocaleString();
	uvDateTime.push(dateString);
}

var uvHistogram =<c:out value="${model.uvHistogram}"/>;
var myChart = new Chart(ctx, {
  type: 'line',
  data: {
    labels: uvDateTime,
    datasets: [{
      label: 'Joules/Square Meter',
      data: uvHistogram,
      backgroundColor: "rgba(153,255,51,0.6)"
    }]
  }
});
</script>


</body>

</html>
