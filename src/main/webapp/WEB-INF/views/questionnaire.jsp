<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>

<%@ include file="/WEB-INF/views/layout.jsp"%>

<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>UV Questionnaire</title>

<link rel="stylesheet"
	href='<c:url value="/resources/assets/css/bootstrap.min.css" />' />
<link rel="stylesheet"
	href='<c:url value="/resources/assets/css/borderless.css" />' />
</head>

<body>
	<div class="site-wrapper">
		<div class="container">
			<div class="info-box" id="intro">
				<h1>UV Monitor's Questionnaire:</h1>
				<hr>

				<div class="row">
					<div class="col-md-8 col-md-offset-2">
					
						<div id='introText'>
							<h2>Welcome to UV Monitor!</h2>
							<p>								
								The following contains a series of 4 questions to answer.<br>
								Answer these questions as correctly as you can so we can build a
								personal UV monitor for your lifestyle.<br> <br> You
								can go back or forward using the arrows on the side.<br> You
								can exit the questionnaire at any time by clicking home at the
								top of the page.<br>
							</p>
						</div>
						
						<div id='questionOne'>
							<h3>Question One:</h3>
							<p>Which of these pictures best matches your daily activity
								or job style the best?</p>
							<div class="row">
								<div class="col-md-3">
									<label>
										<input class="answers" type="radio" value="3"/>
										<img src='<c:url value="/resources/assets/img/questionPics/q1Labor.jpg" />' >
									</label>
									<p>Labor Worker</p>
								</div>								
								<div class="col-md-3">
									<label>
										<input class="answers" type="radio" value="2"/>
										<img src='<c:url value="/resources/assets/img/questionPics/q1Student.jpg" />' >
									</label>
									<p>University/School Student</p>
								</div>
								<div class="col-md-3">
									<label>
										<input class="answers" type="radio" value="2.5"/>
										<img src='<c:url value="/resources/assets/img/questionPics/q1Parent.jpg" />' >
									</label>
									<p>Stay-at-home parent</p>
								</div>
								<div class="col-md-3">
									<label>
										<input class="answers" type="radio" value="4.5"/>								
										<img src='<c:url value="/resources/assets/img/questionPics/q1Office.jpg" />' >
									</label>
									<p>Office Worker</p>
								</div>
							</div>
						</div>
						
						<div id='questionTwo'>
							<h3>Question Two:</h3>
							<p>Which of these pictures best matches how what type of
								exercise you get?</p>
							<div class="row">
								<div class="col-md-3">
									<label>
										<input class="answers" type="radio" value="4"/>
										<img src='<c:url value="/resources/assets/img/questionPics/q2Outdoors.jpg" />' >
									</label>
									<p>Outdoor Workout</p>
								</div>								
								<div class="col-md-3">
									<label>
										<input class="answers" type="radio" value="2"/>
										<img src='<c:url value="/resources/assets/img/questionPics/q2Gym.jpg" />' >
									</label>
									<p>Gym Workout</p>
								</div>
								<div class="col-md-3">
									<label>
										<input class="answers" type="radio" value="3"/>
										<img src='<c:url value="/resources/assets/img/questionPics/Q2Both.jpg" />' >
									</label>
									<p>Gym + Outdoor Workout</p>
								</div>
								<div class="col-md-3">
									<label>
										<input class="answers" type="radio" value="5"/>								
										<img src='<c:url value="/resources/assets/img/questionPics/q2Indoor.jpg" />' >
									</label>
									<p>Little to no activity</p>
								</div>
							</div>
						</div>
						
						<div id='questionThree'>
							<h3>Question Three:</h3>
							<p>Have either you or anyone in your immediate family had a
								history with skin cancer or cancer?</p>
							<div class="row">
								<div class="col-md-2 col-md-offset-1">
									<label>
										<input class="answers" type="radio" value="5"/>
										<img src='<c:url value="/resources/assets/img/questionPics/Q3both.jpg" />' >
									</label>
									<p>Personal + Family History</p>
								</div>	
								<div class="col-md-2">
									<label>
										<input class="answers" type="radio" value="4"/>
										<img src='<c:url value="/resources/assets/img/questionPics/Q3Personal.jpg" />' >
									</label>
									<p>Personal History</p>
								</div>								
								<div class="col-md-2">
									<label>
										<input class="answers" type="radio" value="4"/>
										<img src='<c:url value="/resources/assets/img/questionPics/Q3family.jpg" />' >
									</label>
									<p>Family History</p>
								</div>
								<div class="col-md-2">
									<label>
										<input class="answers" type="radio" value="2"/>
										<img src='<c:url value="/resources/assets/img/questionPics/q3No.png" />' >
									</label>
									<p>No history</p>
								</div>
								<div class="col-md-2">
									<label>
										<input class="answers" type="radio" value="3"/>								
										<img src='<c:url value="/resources/assets/img/questionPics/q3No.png" />' >
									</label>
									<p>Prefer not to disclose</p>
								</div>
							</div>
						</div>
						<div id='questionFour'>
							<h3>Question Four:</h3>
							<p>Which of these pictures best matches your skin tone?</p>
							<div class="row">
								<div class="col-md-2">
									<label>
										<input class="answers" type="radio" value="5"/>
										<img src='<c:url value="/resources/assets/img/questionPics/Q4Pale.jpg" />' >
									</label>
								</div>	
								<div class="col-md-2">
									<label>
										<input class="answers" type="radio" value="4.5"/>
										<img src='<c:url value="/resources/assets/img/questionPics/Q4White.jpg" />' >
									</label>
								</div>								
								<div class="col-md-2">
									<label>
										<input class="answers" type="radio" value="4"/>
										<img src='<c:url value="/resources/assets/img/questionPics/Q4Tanned.jpg" />' >
									</label>
								</div>
								<div class="col-md-2">
									<label>
										<input class="answers" type="radio" value="3"/>
										<img src='<c:url value="/resources/assets/img/questionPics/Q4Brown.jpg" />' >
									</label>
								</div>
								<div class="col-md-2">
									<label>
										<input class="answers" type="radio" value="2"/>								
										<img src='<c:url value="/resources/assets/img/questionPics/Q4DarkBrown.jpg" />' >
									</label>
								</div>
								<div class="col-md-2">
									<label>
										<input class="answers" type="radio" value="1"/>								
										<img src='<c:url value="/resources/assets/img/questionPics/Q4Black.jpg" />' >
									</label>
								</div>								
							</div>
						</div>
						
						<div id='thankYou'>
							<p>								
								Thank you for completing the questionnaire!<br>
								Please press the exit button below so we may save your progress.
							</p>
							<br>
							<div class='col-sm-offset-5 col-sm-2 text-center'>
								<form name='f' method='POST'>								
									<input type='text' name='submitScore' id='scoreBoard'>
									<input name="Return" type="submit" value="submit" />									
								</form>
							</div>						
						</div>
						
					</div>
				</div>

				<div class="panel-footer row" style="">
					<div class="col-xs-3 text-left">
						<div class="previous">
							<button type="button" id="prevShow" class="btn btn-default btn-lg" value="100">
								<span class="glyphicon glyphicon-chevron-left"></span>
							</button>
						</div>
					</div>

					<div class="active col-xs-6 text-middle" style="margin-top:15px;">
						<div class="progress progress-striped active">
							<div class="progress-bar" role="progressbar" aria-valuenow="0"
								aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
						</div>
					</div>
					<div class="col-xs-3 text-right">
						<div class="next">
							<button type="button" id="nextShow" class="btn btn-default btn-lg" value="100">
								<span class="glyphicon glyphicon-chevron-right"></span>
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript"
		src="<c:url value="/resources/assets/js/jquery-3.1.1.slim.js" />"></script>
	<script type="text/javascript"
		src="<c:url value="/resources/assets/js/bootstrap.js" />"></script>
</body>

<script>
	
	var globalScore = 0;
	var finalScore = 0;
	
	function calcRiskScore() {
		$('.answers').on('change', function(){
			$('.answers').not(this).prop('checked', false);
			globalScore = parseFloat($(this).val());
			//console.log("Value selected worth: " + parseFloat($(this).val()));
		})
	}	

	$(function() {		
		jQuery('#questionOne, #questionTwo, #questionThree, #questionFour, #thankYou, #prevShow').hide();
		jQuery('#scoreBoard').hide();
		var scoreTally = [];
		var prog = 0;
		
		for (i = 0; i < 4; i++) {
			scoreTally[i] = 0;
		}
		
		jQuery('#nextShow').on('click',function(event) {
			console.log("score = " + globalScore);
			
			if ($('#introText').is(':visible')) {				
				jQuery('#introText, #questionOne, #prevShow').toggle('show');
				calcRiskScore();
			} else if ($('#questionOne').is(':visible')) {
				scoreTally[0] = globalScore;
				jQuery('#questionOne, #questionTwo').toggle('show');
				calcRiskScore();				
			} else if ($('#questionTwo').is(':visible')) {
				scoreTally[1] = globalScore;
				jQuery('#questionTwo, #questionThree').toggle('show');
				calcRiskScore();				
			} else if ($('#questionThree').is(':visible')) {
				scoreTally[2] = globalScore;
				jQuery('#questionThree, #questionFour').toggle('show');
				calcRiskScore();
			} else {
				scoreTally[3] = globalScore;
				jQuery('#questionFour,#thankYou,#nextShow').toggle('show');
				finalScore = (scoreTally[3] + scoreTally[2] + scoreTally[1] + scoreTally[0]);
				$("input[name=submitScore]").val(finalScore);
			}
			
			globalScore = 0;
			prog += parseInt(this.value);
			$('.progress-bar').attr('aria-valuenow', prog).css('width', prog);
			//console.log("Score: " + scoreTally[0] + "," + scoreTally[1] + "," + scoreTally[2] + "," + scoreTally[3]);
		});
		
		jQuery('#prevShow').on('click',function(event) {
			if ($('#questionOne').is(':visible')) {
				jQuery('#questionOne, #introText, #prevShow').toggle('show');
			} else if ($('#questionTwo').is(':visible')) {
				jQuery('#questionTwo, #questionOne').toggle('show');				
			} else if ($('#questionThree').is(':visible')) {
				jQuery('#questionThree, #questionTwo').toggle('show');
			} else if ($('#questionFour').is(':visible')) {
				jQuery('#questionFour,#questionThree').toggle('show');
			} else if ($('#thankYou').is(':visible')) {
				jQuery('#thankYou, #questionFour, #nextShow').toggle('show');
			}
			
			prog -= parseInt(this.value);
			$('.progress-bar').attr('aria-valuenow', prog).css("width", prog);
			//console.log("Score: " + scoreTally[0] + "," + scoreTally[1] + "," + scoreTally[2] + "," + scoreTally[3]);
		});
		
	});
</script>

<style>
.container {
	margin-right: auto;
	margin-left: auto;
	margin-bottom: 20px;
	padding-left: 15px;
	padding-right: 15px;
}

body {
	background-color: #2386c8;
}

#intro {
	border-radius: 6px;
	padding: 48px 60px;
	background-color: #fff;
}

answers > input {
	visibility: hidden;
	position: absolute;
}

answers > input + img {
	cursor: pointer;
	border: 2px solid transparent;
}

answers  > input:checked + img{
	border: 2px solid #f00;
}

img{
max-width:100%;
max-height:auto%;
}
</style>
</html>

