<html>
<head></head>
<body>


<%@ include file="/WEB-INF/views/layout.jsp" %> 

<div class="log-form" style="text-align: center">
   <h1>Login</h1>
   <!-- REPLACE "LOGIN" WITH TARGET FOR POST DATA -->
   <form name='f' action="login" method='POST' style="text-align: center">
      <table>
         <tr>
            <td>Email Address:</td>
            <td><input type='email' name='email' value=''></td>
         </tr>
         <tr>
            <td>Password:</td>
            <td><input type='password' name='password' /></td>
         </tr>
         <tr>
            <td><button class="pure-button pure-button-primary" name="submit" type="submit" value="login">Login</button></td>
         </tr>
      </table>
  </form>
 </div>
 
  ${model.Footer} 
  
</body>
</html>