<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>
	<title>Home</title>
</head>
<body>

<%@ include file="/WEB-INF/views/layout.jsp" %> 

<div class="text-box">

	<div class="pure-g">
	
		<div class="pure-u-1-1 pure-u-md-1-2">
			<h1>
				UV Monitoring App
			</h1>
			
			<p><fmt:message key="greeting"/><c:out value="${model.now}"/></p>
		</div>
		
		<div class="pure-u-1-1 pure-u-md-1-2">
			
			<p>
			
				This UV Tracking application both monitors your level of both day-to-day and cumulative exposure to UV radiation. 
				This is to aid you in protecting yourself from overexposure or underexposure to UV radiation in order to 
				lower the risk of developing melanoma in their lifetime or Vitamin D deficiency. This positive computing experience hopes to ensure your active health and wellbeing. 
				
			</p>
		
		</div>
	

	</div>


	
	<p>${model.Footer}</p> 
 
 </div>
 <img class="sun" src='<c:url value="/resources/assets/img/sun.jpg" />'/>

 <p>${model.Footer}</p> 

</body>
</html>
