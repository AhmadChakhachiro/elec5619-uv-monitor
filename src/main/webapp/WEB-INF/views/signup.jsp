<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head></head>
<body>

<%@ include file="/WEB-INF/views/layout.jsp" %> 
<div class="log-form" style="text-align: center">
   <h1 style="padding-left: 5%">Sign Up</h1>
   <form name='f' method='POST' action="signup" style="text-align: center">
      <table>
         <tr>
            <td>Email Address:</td>
            <td><input type='email' name='email' value=''></td>
         </tr>
         <tr>
            <td>Password:</td>
            <td><input type='password' name='password' /></td>
         </tr>
         <tr>
            <td><button class="pure-button pure-button-primary" name="submit" type="submit" value="Register!">Register!</button></td>
         </tr>
      </table>
  </form>
</div>
 ${model.Footer} 
 
</body>
</html>