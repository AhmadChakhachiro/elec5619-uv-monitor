package com.elec5619.uvapp.web;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.elec5619.uvapp.domain.User;
import com.elec5619.uvapp.mappings.URLMappings;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.elec5619.uvapp.service.LoginInterface;
import com.elec5619.uvapp.service.SignUpInterface;


/**
 * Handles requests for the application home page.
 */

@Controller
public class LogoutController extends GenericController{
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	
	@Resource(name="loginInterface")
	private LoginInterface loginInterface;
	
	
	@RequestMapping(value = URLMappings.LOGOUT_URL, method = RequestMethod.POST)
	public ModelAndView login(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
		Cookie auth = cookieExtractor("Username",httpServletRequest);
		
		if (auth != null){ //Kill the cookie
			auth.setMaxAge(0);
			httpServletResponse.addCookie(auth);
		} else {
			logger.info("Cookie has escaped punishment");
		}
		
		
		
		return URLMappings.redirect(URLMappings.HOME_URL);
	}
	
}
