package com.elec5619.uvapp.web;

import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.elec5619.uvapp.mappings.HTMLMappings;
import com.elec5619.uvapp.mappings.URLMappings;
import com.elec5619.uvapp.service.BOMInterface;
import com.elec5619.uvapp.domain.UVEntity;

import com.elec5619.uvapp.service.HistoryInterface;
import com.elec5619.uvapp.service.QuestionnaireInterface;

import java.util.logging.Level;

import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestParam;


/**
 * Handles requests for the application home page.
 */

@Controller
public class UVController extends GenericController{
	
	
	private static final Logger logger = LoggerFactory.getLogger(UVController.class);
	
	@Resource(name="BOMInterface")
	private  BOMInterface BOMInterface;
	
	@Resource(name="historyInterface")
	private HistoryInterface historyInterface;
	
	@Resource(name="QuestionnaireInterface")
	private QuestionnaireInterface QuestionnaireInterface;
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	

	
	@RequestMapping(value=URLMappings.UVSINGLE_URL, method = RequestMethod.GET)
	public ModelAndView uvsingle(HttpServletRequest httpServletRequest, @CookieValue(value="Username", required=false) String username) throws Exception {
            
		Map myModel = returnGenericView(httpServletRequest);
                
                if (username!=null)
                    
                {   
                    //user is logged in
                    UVEntity uvEntity = BOMInterface.getUV(1, 1);

                    double uvValue = uvEntity.getUV();
                    logger.info(Double.toString(uvValue));


                    double timeOfDay = timeOfDayDouble();
                    logger.info(Double.toString(timeOfDay));
                    double currentUV = BOMInterface.currentUV(timeOfDay, uvValue);
                    double riskValue = QuestionnaireInterface.getUserRisk(username);

                    uvFooter(myModel, currentUV, riskValue);
                    System.out.println(myModel.get(HTMLMappings.MAP_FOOTER));

                    myModel.put("uvHistogram", BOMInterface.getUVHistogram(uvValue));
                    myModel.put("uvSingle", String.format( "%.2f", currentUV));

                    
                    displayNotification(myModel,username);
                }
                
                
		return authenticationCheck(URLMappings.UVSINGLE_VIEW, "model", myModel, "You must be logged in to view this page" , URLMappings.LOGIN_VIEW);
	}
	
	
	@RequestMapping(value=URLMappings.UVSINGLE_URL, method = RequestMethod.POST)
	public ModelAndView uvCheckIn(@RequestParam("hours") double hours, @CookieValue(value="Username", required=false) String username) throws Exception {
		
		//String username = cookieExtractor("Username", httpServletRequest).getValue();
                
                if (username!=null)
                    {       
                            //USER IS LOGGED IN
                            double currentUv = BOMInterface.currentUV(timeOfDayDouble(), BOMInterface.getUV(1, 1).getUV());

                            logger.info("Check In POST Request");

                            Boolean succeeded = this.historyInterface.checkIn(username,currentUv, hours);

                            if (succeeded) // Insert into database UV data
                                    return URLMappings.redirect(URLMappings.UVHISTORY_VIEW);
                             // Insert failed

                            Map myModel = getGenericViewInterface().getGenericView();
                            popup(myModel, "CONNECTION ERROR");
                            return new ModelAndView(URLMappings.HOME_VIEW, "model", myModel);
                    }
                
                else
			
                    {

                            //USER ISN'T LOGGED IN
                        
                            Map myModel = getGenericViewInterface().getGenericView();
                            popup(myModel, "Your session has expired. Log in and try again.");
                            return new ModelAndView(URLMappings.HOME_VIEW, "model", myModel);

                    }
		
	}
	
	
	private double timeOfDayDouble(){
		String formattedDate = (new java.util.Date()).toString();
		return Double.parseDouble(formattedDate.substring(11,13)) + Double.parseDouble(formattedDate.substring(14,16))/60.0;
	}
	
	
	private void uvFooter(Map myModel, double uv, Double riskValue){
		
		// Need to calculate overall score.
		if (riskValue == null ) {
			riskValue = 10.0;
		}
		
		// Calculate the Final Score based on risk and the current UV.
		double finalScore = (uv * riskValue) / 6;
		System.out.println(Double.toString(finalScore));
			
		String uvMessage = "";
		String riskMessage = "";
		String colour = "blue";
		
		if (uv < 10){
			uvMessage = "The UV levels are too low to be of much use now";
		} else if (uv < 20) {
			uvMessage = "UV levels are moderate; go catch some rays!";
		} else if (uv <= 25){
			uvMessage = "Dont stay outside for too long! Skin cancer doesn't go away.";
		} else {
			uvMessage = "Seek shelter, drink water, melanoma isn't pretty.";
		}
		
		if (finalScore < 20){
			riskMessage =  ", you should probably not be concerned.";
			colour = "blue";
		} else if (finalScore < 40){
			riskMessage = " ... With some sunscreen, you should take care.";
			colour = "green";
		} else if (finalScore <= 60){
			riskMessage = " You might want to consider your risk factor.";
			colour = "orange";
		} else if (finalScore > 80){
			riskMessage = " Don't say we didn't warn you.";
			colour = "red";
		}	
		
		footer(myModel, uvMessage + riskMessage, colour );
			
		return;
	}
        
        private void displayNotification(Map myModel, String theUsername){
            
            
            try {
                Long daysSinceLastCheckIn=historyInterface.getLastCheckIn(theUsername);
                if (daysSinceLastCheckIn!=null)
                {
                    if (daysSinceLastCheckIn>7)
                        popup(myModel, "<p  style=\"color:red;\"><strong>WARNING:</strong></p> You haven't checked in for "+daysSinceLastCheckIn+" days! <br>Check in now in order to prevent a Vitamin D deficiency from occuring!");
                    else 
                    {
                        Double weeklyHours = historyInterface.getWeeklyExposure(theUsername);
                        
                        if (weeklyHours!=null && weeklyHours<2)
                            popup(myModel, "<p><strong>INFO:</strong></p> You need at least 2 hours of sun per week. You last checked in "+daysSinceLastCheckIn+" day(s) ago, <br> and you've only spent "+weeklyHours+" hour(s) outside when the sun was bright enough. <br>Make sure you check in again soon. ");
                            
                    
                    }
                
                
                }
                
                
                
                
            } catch (Exception ex) {
                java.util.logging.Logger.getLogger(UVController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            
		
	}
	
	
}
