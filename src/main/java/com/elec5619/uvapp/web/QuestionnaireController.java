package com.elec5619.uvapp.web;

import java.util.Map;

import javax.annotation.Resource;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestParam;

import com.elec5619.uvapp.mappings.URLMappings;
import com.elec5619.uvapp.service.QuestionnaireInterface;

@Controller
public class QuestionnaireController extends GenericController {

	@Resource(name = "QuestionnaireInterface")
	private QuestionnaireInterface QuestionnaireInterface;

	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

	@RequestMapping(value = URLMappings.QUESTIONNAIRE_URL, method = RequestMethod.GET)
	public ModelAndView questionRequest(HttpServletRequest httpServletRequest, @CookieValue(value = "Username", required = false) String username) throws Exception {

		Map myModel = returnGenericView(httpServletRequest);
		logger.info("POST Request: " + URLMappings.QUESTIONNAIRE_URL);

		return authenticationCheck(URLMappings.QUESTIONNAIRE_VIEW, "model", myModel,
				"You must be logged in to view this page", URLMappings.LOGIN_VIEW);
	}

	@RequestMapping(value = URLMappings.QUESTIONNAIRE_URL, method = RequestMethod.POST)
	public ModelAndView saveScore(HttpServletRequest httpServletRequest, @CookieValue(value="Username", required = false) String username) throws Exception {

		Map myModel = getGenericViewInterface().getGenericView();		
		float newScore = Float.parseFloat(httpServletRequest.getParameter("submitScore"));
		
		logger.info("========= Questionnaire Controller Recieved value " + newScore + " =========");

		if (username != null) {
			// User logged in, continue.
			logger.info("Check In POST request: Questionnaire");
			
			// Check questStatus attribute in user table.
			if (this.QuestionnaireInterface.checkQuestStatus(username) != true) {
				// Then its the first time completing questionnaire, questStatus update flag to true.
				if (this.QuestionnaireInterface.updatedQuestStatus(username) != true) {
					// Something broke
					popup(myModel, "CONNECTION ERROR");
					return URLMappings.redirect(URLMappings.HOME_URL);
				}
				// Otherwise returned true, so we continue with updating scores.
			}
			
			// A value was given, send score to QuestionnaireManager.
			if (this.QuestionnaireInterface.updateRiskFactor(username, newScore) == true) {
				logger.info("RiskFacter POST request success: Questionnaire");
				return URLMappings.redirect(URLMappings.HOME_URL);
			} else {
				// Error happened while trying to update score.
				popup(myModel, "CONNECTION ERROR");
				return URLMappings.redirect(URLMappings.HOME_URL);
			}	
			
		} else {
			// User not logged in, exit out.
			popup(myModel, "Your session has expired. Log in and try again.");
			return new ModelAndView(URLMappings.HOME_VIEW, "model", myModel);
		}
	}

}
