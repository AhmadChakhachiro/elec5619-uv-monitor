package com.elec5619.uvapp.web;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.servlet.ModelAndView;

import com.elec5619.uvapp.mappings.HTMLMappings;
import com.elec5619.uvapp.mappings.URLMappings;
import com.elec5619.uvapp.service.GenericViewInterface;

public abstract class GenericController {
	
	private boolean auth_view = false;
	
	@Resource(name="genericViewInterface")
	private GenericViewInterface genericViewInterface;

	public GenericViewInterface getGenericViewInterface() {
		return genericViewInterface;
	}

	public void setGenericViewInterface(GenericViewInterface genericViewManager) {
		this.genericViewInterface = genericViewManager;
	}
	
	protected Map returnGenericView(){
		Map myModel = this.getGenericViewInterface().getGenericView();
		return myModel;
	}
		
	protected Map returnGenericView(HttpServletRequest httpServletRequest){
		Cookie auth = cookieExtractor("Username", httpServletRequest);
		
		if (auth == null){ // User is not authenticated
			this.auth_view = false;
			return returnGenericView();
		} else {
			System.out.println(auth.getValue());
			this.auth_view = true;
		}

		String username = auth.getValue();
		
		Map myModel = this.getGenericViewInterface().getNonGenericView(username, HTMLMappings.HTML_LOGOUT_MENU,  HTMLMappings.HTML_SIDEBAR_AUTH, null, null);
		return myModel;
	}
	
	// Extract the required cookie because the java API is a pissy little bitch
	protected Cookie cookieExtractor(String name, HttpServletRequest httpServletRequest){
		Cookie[] cookies = httpServletRequest.getCookies();
		
		if (cookies == null){ // There were no cookies in the cookie jar
			return null;
		}
		
		for (int i = 0; i < cookies.length; i++) {  
		  if (cookies[i].getName().equals(name)){
			  return cookies[i];
		  }
		}
		return null;
	}
	
	protected void popup(Map myModel, String popup){
		popup = HTMLMappings.popupGenerator(popup);
		myModel.put(HTMLMappings.MAP_POPUP, popup);
	}
	
	protected void footer(Map myModel, String footer, String colour){
		footer = HTMLMappings.footerGenerator(footer, colour);
		myModel.put(HTMLMappings.MAP_FOOTER, footer);
	}
	
	
	// If the user is authenticated, this returns the correct view
	// returnGeneticView must be run before this command for any page that requires authentication
	protected ModelAndView authenticationCheck(String view, String map_name, Map myModel, String popup, String defaultView){
		System.out.println("auth status: " + Boolean.toString(this.auth_view));
		if (this.auth_view){
			return new ModelAndView(view, map_name, myModel);
		} else {
			popup(myModel, popup);
			return new ModelAndView(defaultView, map_name, myModel);
		}
		
	}
	
	
}
