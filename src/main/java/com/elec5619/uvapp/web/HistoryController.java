package com.elec5619.uvapp.web;

import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.elec5619.uvapp.mappings.URLMappings;
import com.elec5619.uvapp.service.BOMInterface;
import com.elec5619.uvapp.domain.UVEntity;

import com.elec5619.uvapp.service.HistoryInterface;

import java.util.logging.Level;


import org.springframework.web.bind.annotation.CookieValue;


/**
 * Handles requests for the application home page.
 */

@Controller
public class HistoryController extends GenericController{
	
	
	private static final Logger logger = LoggerFactory.getLogger(HistoryController.class);
	
	@Resource(name="BOMInterface")
	private  BOMInterface BOMInterface;
	
	@Resource(name="historyInterface")
	private HistoryInterface historyInterface;
	
	/**
	 * Method for checking the current uv History page
	 */
	

	
	@RequestMapping(value=URLMappings.UVHISTORY_URL, method = RequestMethod.GET)
	public ModelAndView uvsingle(HttpServletRequest httpServletRequest, @CookieValue(value="Username", required=false) String username) throws Exception { //Use cookie value notation to check cookie
            
		Map myModel = returnGenericView(httpServletRequest);
                
                if (username!=null)
                {
                     //user is logged in
                    
                    UVEntity uvEntity = BOMInterface.getUV(1, 1);
                    //String username = cookieExtractor("Username", httpServletRequest).getValue();


                    double uvValue = uvEntity.getUV();
                    logger.info(Double.toString(uvValue));


                    double timeOfDay = timeOfDayDouble();
                    logger.info(Double.toString(timeOfDay));
                    double currentUV = BOMInterface.currentUV(timeOfDay, uvValue);

                    logger.info("Getting History Data");
                    String[] historyData = historyInterface.getHistory(username);

                    //byte[] timeData = historyData[1].getBytes(StandardCharsets.US_ASCII); 



                    if (historyData == null ){ // No datapoints
                            popup(myModel,"You must first check in to see your UV history");
                            return uvsingle(httpServletRequest, myModel);
                    }

                    myModel.put("uvHistogram", historyData[0]);
                    myModel.put("timedata", historyData[1]);
                    myModel.put("uvSingle", String.format( "%.2f", currentUV));

                    displayNotification(myModel, username);
                }
		
		return authenticationCheck(URLMappings.UVHISTORY_VIEW, "model", myModel, "You must be logged in to view this page" , URLMappings.LOGIN_VIEW);
	}
	
	
	private double timeOfDayDouble(){
		String formattedDate = (new java.util.Date()).toString();
		return Double.parseDouble(formattedDate.substring(11,13)) + Double.parseDouble(formattedDate.substring(14,16))/60.0;
	}
	
	
	
	
	private ModelAndView uvsingle(HttpServletRequest httpServletRequest, Map myModel) throws Exception {
		UVEntity uvEntity = BOMInterface.getUV(1, 1);
		
		double uvValue = uvEntity.getUV();
		logger.info(Double.toString(uvValue));
		
		
		double timeOfDay = timeOfDayDouble();
		logger.info(Double.toString(timeOfDay));
		double currentUV = BOMInterface.currentUV(timeOfDay, uvValue);
		
		
		myModel.put("uvHistogram", BOMInterface.getUVHistogram(uvValue));
		myModel.put("uvSingle", String.format( "%.2f", currentUV));
		return authenticationCheck(URLMappings.UVSINGLE_VIEW, "model", myModel, "You must be logged in to view this page" , URLMappings.LOGIN_VIEW);
	}
        
        
        private void displayNotification(Map myModel, String theUsername){
            
            
            try {
                Double totalUV = historyInterface.getCumulativeUV(theUsername, false);
                Double monthlyUV = historyInterface.getCumulativeUV(theUsername, true);
                
                String formattedTotal = totalUV!=null ? String.format("%.2f", totalUV) : "ERROR";
                String formattedmonthlyTotal = monthlyUV!=null ? String.format("%.2f", monthlyUV) : "ERROR";
                
                String info="";
                if (totalUV!=null && monthlyUV!=null)
                    info = totalUV>205000 || monthlyUV>350 ? "<p  style=\"color:red;\"><strong>WARNING!!!</strong></p>" : "<p><strong>INFO:</strong></p>";
                
                popup(myModel, info+"Total Cumulative UV index level (CUES): "+formattedTotal+" / 205000 units <br> Monthly Cumulative UV index level (CUES): "+formattedmonthlyTotal+" / 350 units");
            } catch (Exception ex) {
                java.util.logging.Logger.getLogger(HistoryController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            
		
	}
	
	
	
}
