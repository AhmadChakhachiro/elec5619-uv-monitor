package com.elec5619.uvapp.web;

import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.elec5619.uvapp.domain.UVEntity;
import com.elec5619.uvapp.mappings.URLMappings;



/**
 * Handles requests for the application home page.
 */

@Controller
public class HomeController extends GenericController{
	
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	
	@RequestMapping(value = URLMappings.HOME_URL, method = RequestMethod.GET)
	public ModelAndView home(HttpServletRequest httpServletRequest) throws Exception {
		Map myModel = returnGenericView(httpServletRequest);
		
		String formattedDate = (new java.util.Date()).toString();
		logger.info("Welcome home! The client locale is {}.", formattedDate);
		myModel.put("now", formattedDate);
		

		return authenticationCheck(URLMappings.HOMEAUTH_VIEW, "model", myModel, "", URLMappings.HOME_VIEW);
	}
	
	
	//Map index to the home page
	@RequestMapping(value = URLMappings.HOMEB_URL, method = RequestMethod.GET)
	public ModelAndView homeb(HttpServletRequest httpServletRequest) throws Exception{
		return home(httpServletRequest);
	}
	
}
