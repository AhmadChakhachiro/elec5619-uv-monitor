package com.elec5619.uvapp.web;

import java.util.Map;

import javax.annotation.Resource;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.elec5619.uvapp.domain.User;
import com.elec5619.uvapp.mappings.URLMappings;
import com.elec5619.uvapp.service.SignUpInterface;


@Controller
public class SignUpController extends GenericController {
	
	
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	@Resource(name="signUpInterface")
	private SignUpInterface signUpInterface;
	
	@RequestMapping(value="/signup", method = RequestMethod.GET)
	public ModelAndView signup(Model model) {
		
		logger.info("Sign Up GET Request");
		
		Map myModel = this.getGenericViewInterface().getGenericView();
		return new ModelAndView(URLMappings.SIGNUP_VIEW, "model", myModel);
	}
	
	@RequestMapping(value="/signup", method=RequestMethod.POST)
	public ModelAndView addUser(HttpServletRequest httpServletRequest) throws Exception {
		
		logger.info("Sign Up POST Request");
		
		String userEmail = httpServletRequest.getParameter("email");
		String userPassword = httpServletRequest.getParameter("password");
		
		
		Boolean succeeded = this.signUpInterface.signUp(userEmail, userPassword);
		
		if (succeeded){ // User was created successfully
			return URLMappings.redirect(URLMappings.HOME_URL);
		} //Another user with that name exists
		
		Map myModel = getGenericViewInterface().getGenericView();
		popup(myModel, "This username has already been taken");
		return new ModelAndView(URLMappings.SIGNUP_VIEW, "model", myModel);
		
	}
}
