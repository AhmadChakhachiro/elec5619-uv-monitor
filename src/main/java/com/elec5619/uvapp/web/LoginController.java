package com.elec5619.uvapp.web;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.elec5619.uvapp.domain.User;
import com.elec5619.uvapp.mappings.URLMappings;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.elec5619.uvapp.service.LoginInterface;
import com.elec5619.uvapp.service.SignUpInterface;
import com.elec5619.uvapp.service.QuestionnaireInterface;

/**
 * Handles requests for the application home page.
 */

@Controller
public class LoginController extends GenericController {

	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

	@Resource(name = "loginInterface")
	private LoginInterface loginInterface;
	
	@Resource(name = "QuestionnaireInterface")
	private QuestionnaireInterface QuestionnaireInterface;

	@RequestMapping(value = URLMappings.LOGIN_URL, method = RequestMethod.GET)
	public ModelAndView login(HttpServletRequest httpServletRequest) throws Exception {
		Map myModel = returnGenericView(httpServletRequest);

		// logger.info(myModel.get("header").toString());

		return new ModelAndView(URLMappings.LOGIN_VIEW, "model", myModel);

	}

	@RequestMapping(value=URLMappings.LOGIN_URL, method=RequestMethod.POST)
	public ModelAndView login(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
		logger.info("-----");
		logger.info("POST Request:" + URLMappings.LOGIN_URL);
		logger.info("-----");
		String userEmail = httpServletRequest.getParameter("email");
		String userPassword = httpServletRequest.getParameter("password");
		
		Boolean validated = loginInterface.login(userEmail, userPassword);
		
		if (validated){
			
			logger.info("User Validated");
			
			Cookie username = new Cookie("Username", userEmail);
			httpServletResponse.addCookie(username);
			// Validated user, checking to see if Questionnaire complete.
			if (this.QuestionnaireInterface.checkQuestStatus(userEmail) == false) {
				// If false redirect to questionnaire page.
				logger.info("User with incomplete Questionnaire, redirecting to questionnaire page.");
				return URLMappings.redirect(URLMappings.QUESTIONNAIRE_URL);
			} else {
				return URLMappings.redirect(URLMappings.HOME_URL);
			}
		}
		else{
			logger.info("User failed to Validate");
			Map myModel = returnGenericView(httpServletRequest);
			popup(myModel, "Invalid Username or Password");
			return new ModelAndView(URLMappings.LOGIN_VIEW, "model", myModel);
		}
		
	}
	

	
}
