package com.elec5619.uvapp.mappings;

import org.springframework.web.servlet.ModelAndView;

public final class URLMappings {
	
	public static final String LOGIN_URL = "/login";
	public static final String LOGIN_VIEW = "login";
	
	public static final String LOGOUT_URL = "/logout";
	public static final String LOGOUT_VIEW = "logout";
	
	public static final String HOME_URL = "";
	public static final String HOMEB_URL = "index";
	public static final String HOME_VIEW = "home";
	
	public static final String HOMEAUTH_VIEW = "home_auth";
	
	public static final String SIGNUP_URL = "/signup";
	public static final String SIGNUP_VIEW = "signup";
	
	public static final String UVSINGLE_URL = "/uvsingle";
	public static final String UVSINGLE_VIEW = "uvsingle";
	
	public static final String UVINFO_URL = "/uvinfo";
	public static final String UVINFO_VIEW = "uvinfo";
	
	public static final String UVHISTORY_URL = "/uvhistory";
	public static final String UVHISTORY_VIEW = "uvhistory";
	
	public static final String QUESTIONNAIRE_URL = "/questionnaire";
	public static final String QUESTIONNAIRE_VIEW = "questionnaire";
	
	private URLMappings(){}
	
	
	
	public static ModelAndView redirect(String url){
		return new ModelAndView("redirect:/" + url);
	}
	
}
