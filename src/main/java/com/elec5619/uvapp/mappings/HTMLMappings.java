package com.elec5619.uvapp.mappings;

import org.springframework.web.servlet.ModelAndView;

public final class HTMLMappings {
	
	private HTMLMappings(){}
	
	// Wraps the popups 
	public static final String[] HTML_POPUP = {"<div class='pure-g'><div class='pure-u-1-1' style='background-color:orange; color:white; text-align:center;'><p>","</p></div></div>"};
	public static final String[] HTML_FOOTER = {"<div class='pure-g'><div class='pure-u-1-1' style='background-color:","; color:white; text-align:center;'><p>","</p></div></div>"};

	
	// Sidebar
	public static final String HTML_UVINFO_BUTTON =  menuItemGenerator(URLMappings.UVINFO_VIEW, "UV Information");
	public static final String HTML_UVSINGLE_BUTTON =  menuItemGenerator(URLMappings.UVSINGLE_VIEW, "Daily UV Infographic");
	public static final String HTML_UVHISTORY_BUTTON = menuItemGenerator(URLMappings.UVHISTORY_VIEW, "My UV History");
	public static final String HTML_UVQUESTION_BUTTON = menuItemGenerator(URLMappings.QUESTIONNAIRE_VIEW, "Your Questionnaire");
		
	public static final String HTML_SIDEBAR = HTML_UVINFO_BUTTON;
	public static final String HTML_SIDEBAR_AUTH = HTML_UVINFO_BUTTON + HTML_UVSINGLE_BUTTON + HTML_UVHISTORY_BUTTON + HTML_UVQUESTION_BUTTON;
	
	// Home Button
	public static final String HOME_BUTTON = menuItemGenerator(URLMappings.HOMEB_URL, "Home");
			
	// Logout Button
	public static final String HTML_LOGOUT_BUTTON = buttonGenerator(URLMappings.LOGOUT_VIEW, "Logout", "POST"); 
	
	public static final String HTML_LOGOUT_MENU = HOME_BUTTON + "<li class='pure-menu-item'>" + HTML_LOGOUT_BUTTON + "</li>";
	
	
	
	// Mapping Names
	public static final String MAP_HEADER = "Header";
	public static final String MAP_BANNER = "Banner";
	public static final String MAP_SIDEBAR = "Sidebar";
	public static final String MAP_POPUP = "Popup";
	public static final String MAP_FOOTER = "Footer";
	
	
	public static String menuItemGenerator(String target, String text){
		String head = "<li class='pure-menu-item'><a href='";
		String middle = "' class='pure-menu-link'>";
		String tail = "</a></li>";
		return head + target + middle + text + tail;
	}
	
	public static String buttonGenerator(String target, String text){
		String head = "<form name='f' action='";
		String middle = "' method='GET'> <input name='submit' type='submit' value='";
		String tail = "'/></form>";
		return head + target + middle + text + tail;
	}
	
	public static String buttonGenerator(String target, String text, String method){
		String breakfast = "<form name='f' action='";
		String secondBreakfast = "' method='";
		String morningTea = "'> <button class='pure-button pure-button-primary' name='submit' type='submit' value='";
		String elevensies = "'>";
		String brunch = "</button>";
		String lunch = "</form>";
		return breakfast + target + secondBreakfast + method + morningTea + text + elevensies + text + brunch + lunch;
	}
	
	public static String popupGenerator(String text){
		if (text != ""){
			return HTMLMappings.HTML_POPUP[0] + text + HTMLMappings.HTML_POPUP[1];
		}
		return "";
	}
		
	public static String footerGenerator(String text, String colour){
		if (colour == ""){
			colour = "blue";
		}
		
		if (text != ""){
			return HTMLMappings.HTML_FOOTER[0] + colour + HTMLMappings.HTML_FOOTER[1] + text + HTMLMappings.HTML_FOOTER[2];
		}
		return "";
	}
	
	
	
}
