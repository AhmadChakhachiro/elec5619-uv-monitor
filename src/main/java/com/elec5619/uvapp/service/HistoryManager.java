package com.elec5619.uvapp.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.elec5619.uvapp.domain.HistoryEntity;
import com.elec5619.uvapp.domain.UVEntity;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;

//Database imports

import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CookieValue;

@Service(value = "historyInterface")
@Transactional
public class HistoryManager implements HistoryInterface {

	private static final Logger logger = LoggerFactory.getLogger(HistoryManager.class);
	private SessionFactory sessionFactory;

	@Autowired
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	@Override
	public Boolean checkIn(String email, double uv, double hours) throws Exception {

		HistoryEntity hist = new HistoryEntity(email, uv, hours);

		try {
			if (sessionFactory == null) {
				System.out.println("Session factory broken, connection error");
				throw new Exception();
			} else {
				this.sessionFactory.getCurrentSession().save(hist);
				return true;
			}
		} catch (Exception e) {
			logger.info(e.toString());
		}

		return false;
	}

	public String[] getHistory(String email) throws Exception {

		List<HistoryEntity> uvHistory = getUVHistory(email);

		if (uvHistory == null) 
			return null;

		if (uvHistory == null)
			return null;

		if (uvHistory.size() == 0) {

			return null;
		}

		String[] output = getUVHistogram(uvHistory);
		return output;
	}

	private List<HistoryEntity> getUVHistory(String email) throws Exception {
		List<HistoryEntity> uvHistory = null;
		try { // Make sure the session factory is working
			if (this.sessionFactory == null) { // Session factory has not
												// crashed but is void (server
												// down?)
				logger.info("Session Factory Failed");
				throw new Exception();
			} else { // Session factory is up, running and connected

				// Query the Database
				String query_text = "SELECT * FROM userdata WHERE email = :email";
				Session currentSession = this.sessionFactory.getCurrentSession();
				SQLQuery query = currentSession.createSQLQuery(query_text);
				query.addEntity(HistoryEntity.class);
				query.setString("email", email);
				uvHistory = (List<HistoryEntity>) query.list();
			}
		} catch (Exception e) {
			logger.info(e.toString());
		}

		return uvHistory;
	}

	public String[] getUVHistogram(List<HistoryEntity> uvHistory) throws ParseException {

		String uvData = "[";
		String timestampData = "[";

		for (HistoryEntity entity : uvHistory) {
			uvData = uvData.concat(String.format("%.2f", entity.getUV()));
			timestampData = timestampData.concat(getEpochTime(entity.getTimestamp()));
			uvData += ", ";
			timestampData += ", ";
		}

		uvData = uvData.substring(0, uvData.length() - 2);
		timestampData = timestampData.substring(0, timestampData.length() - 2);

		uvData += "]";
		timestampData += "]";

		String[] output = { uvData, timestampData };
		return output;
	}

	private String getEpochTime(String datetime) throws ParseException {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
		logger.info(datetime);
		Date date = df.parse(datetime);
		logger.info("parsed time " + date);
		long epoch = date.getTime();
		return Long.toString(epoch);
	}

	public Double getCumulativeUV(String email, boolean month) throws Exception {

		try { // Make sure the session factory is working
			if (this.sessionFactory == null) { // Session factory has not
												// crashed but is void (server
												// down?)
				logger.info("Session Factory Failed");
				throw new Exception();
			} else { // Session factory is up, running and connected
				Session currentSession = this.sessionFactory.getCurrentSession();
				List<Object[]> uvlist = currentSession
						.createQuery("select uv,hours,dateTime from HistoryEntity where email=:email")
						.setParameter("email", email).list();

				Double total = 0.0;
				LocalDateTime currenttime = LocalDateTime.now();

				for (Object[] item : uvlist) {
					Double uv = (Double) item[0];
					uv = uv * 0.4; // converting the joules value to the index
									// value
					Double hours = (Double) item[1];
					String dtime = (String) item[2];

					DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S");
					LocalDateTime thedate = LocalDateTime.parse(dtime, formatter);

					if (month)

					{
						if (thedate.getMonth() == currenttime.getMonth() && thedate.getYear() == currenttime.getYear())
							total += uv * hours;
					}

					else
						total += uv * hours;
				}

				return total;
			}
		} catch (Exception e) {
			logger.info(e.toString());
			return null;
		}

	}

	public Double getWeeklyExposure(String email) throws Exception {

		try { // Make sure the session factory is working
			if (this.sessionFactory == null) { // Session factory has not
												// crashed but is void (server
												// down?)
				logger.info("Session Factory Failed");
				throw new Exception();
			} else { // Session factory is up, running and connected
				Session currentSession = this.sessionFactory.getCurrentSession();
				List<Object[]> uvlist = currentSession
						.createQuery("select uv,hours,dateTime from HistoryEntity where email=:email")
						.setParameter("email", email).list();

				Double total = 0.0;
				LocalDateTime currenttime = LocalDateTime.now();

				for (Object[] item : uvlist) {
					Double uv = (Double) item[0];
					Double hours = (Double) item[1];
					String dtime = (String) item[2];

					DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S");
					LocalDateTime thedate = LocalDateTime.parse(dtime, formatter);

					if (Duration.between(thedate, currenttime).toDays() < 8 && uv > 10) // count
																						// the
																						// hours
																						// only
																						// if
																						// it
																						// was
																						// recorded
																						// when
																						// the
																						// sun
																						// was
																						// up,
																						// i.e.,
																						// with
																						// a
																						// UV
																						// value
																						// of
																						// at
																						// least
																						// 10
						total += hours;

				}

				return total; // returns the total hours of sun the user got
								// during the past week
			}
		} catch (Exception e) {
			logger.info(e.toString());
			return null;
		}

	}


	public Long getLastCheckIn(String email) throws Exception {

		try { // Make sure the session factory is working
			if (this.sessionFactory == null) { // Session factory has not
												// crashed but is void (server
												// down?)
				logger.info("Session Factory Failed");
				throw new Exception();
			} else { // Session factory is up, running and connected
				System.out.println("CHECK-IN STATUS");
				// Query the Database
				Session currentSession = this.sessionFactory.getCurrentSession();
				String checkin = (String) currentSession
						.createQuery("select dateTime from HistoryEntity where email=:email order by dateTime desc")
						.setParameter("email", email).list().get(0);

				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S");
				LocalDateTime dateTime = LocalDateTime.parse(checkin, formatter);
				Long daysSinceLastCheckIn = Duration.between(dateTime, LocalDateTime.now()).toDays();
				System.out.println("Last check in was " + daysSinceLastCheckIn + " days ago");

				return daysSinceLastCheckIn;

			}
		} catch (Exception e) {
			logger.info(e.toString());
			return null;
		}

	}

}
