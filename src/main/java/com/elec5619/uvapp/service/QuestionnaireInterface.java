package com.elec5619.uvapp.service;

import java.io.Serializable;

import org.hibernate.SessionFactory;

public interface QuestionnaireInterface extends Serializable{
	
	public boolean checkQuestStatus(String username) throws Exception;
	
	public Boolean updatedQuestStatus(String username) throws Exception;
	
	public Boolean updateRiskFactor(String email, double riskValue) throws Exception;
	
	public Double getUserRisk(String email) throws Exception;

}
