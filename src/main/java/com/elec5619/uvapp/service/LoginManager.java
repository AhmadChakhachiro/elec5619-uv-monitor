package com.elec5619.uvapp.service;

import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import com.elec5619.uvapp.domain.User;
import com.elec5619.uvapp.service.internal.UserManager;

import org.springframework.beans.factory.annotation.Autowired;

//Database imports

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service(value="loginInterface")
@Transactional
public class LoginManager implements LoginInterface {
	
	private static final Logger logger = LoggerFactory.getLogger(LoginManager.class);
	private UserManager userManager = new UserManager();
	private SessionFactory sessionFactory;
	
	@Autowired
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	@Override
	public Boolean login(String email, String password) throws Exception {
		Boolean validated = false;

		User user = this.userManager.getUser(email, this.sessionFactory);
		
		if (user != null){ // A single user with the corresponding email was found
			validated = user.login(password);	
			logger.info("User has been "+ validated.toString());
		} 
		return validated;
	}
}

