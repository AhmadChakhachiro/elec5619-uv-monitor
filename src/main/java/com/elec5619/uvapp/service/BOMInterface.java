package com.elec5619.uvapp.service;

import java.io.Serializable;

import com.elec5619.uvapp.domain.UVEntity;


public interface BOMInterface extends Serializable{
	
	public UVEntity getUV (double latitude, double longitude)throws Exception;
	
	public String getUVHistogram(double UV);
	
	public double currentUV(double time, double dailyUV);
	
}
