package com.elec5619.uvapp.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.elec5619.uvapp.domain.UVEntity;
import com.elec5619.uvapp.domain.User;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;

//Database imports

import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service(value="BOMInterface")
@Transactional
public class BOMManager implements BOMInterface{
	
	private static final Logger logger = LoggerFactory.getLogger(LoginManager.class);
	private SessionFactory sessionFactory;
	
	@Autowired
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	public UVEntity getUV (double latitude, double longitude) throws Exception {
		UVEntity uv = null;
		try {			// Make sure the session factory is working	
			if(this.sessionFactory==null){ 	// Session factory has not crashed but is void (server down?)
				logger.info("Session Factory Failed");
				throw new Exception();
			} 
			else{ //Session factory is up, running and connected
				
				// Query the Database
				String query_text = "SELECT B.location, B.uv FROM BomData as B INNER JOIN (SELECT location, SQRT(POW(latitude - :lat ,2) + POW(longitude - :long,2)) as dist From BomData) as A ON A.location=B.location ORDER BY A.dist LIMIT 1";
				Session currentSession = this.sessionFactory.getCurrentSession();
				SQLQuery query = currentSession.createSQLQuery(query_text);
				query.addEntity(UVEntity.class);
				query.setString("lat", Double.toString(latitude));
				query.setString("long", Double.toString(longitude));
				List result = query.list();
				
				uv = (UVEntity) result.get(0);
	
			}
		} catch (Exception e){
			logger.info(e.toString());
		}
		
		return uv;
	}
	
	public String getUVHistogram(double UV){
		
		String uvHistString = "[";
		for (int i = 0; i < 24; i++){
		
			double x =  currentUV(i, UV);
			uvHistString = uvHistString.concat(String.format( "%.2f", x ) ); 
			
			uvHistString = uvHistString.concat(", ");

		}
		
		uvHistString = uvHistString.substring(0, uvHistString.length() - 2);
		
		uvHistString = uvHistString.concat("]");
		return uvHistString;
	}
	
	public double currentUV(double time, double dailyUV){
		return gauss(time, 12.1, 13, 0.95 * dailyUV) + gauss(time, 13.3, 13, 1.5) + gauss(time, 14, 13, 1) + gauss(time, 15, 13, 0.75);
	}
	
	private double gauss(double x, double mean, double sigma, double amplitude){
		return amplitude * Math.exp(- Math.pow(x - mean, 2)/sigma);
		
	}
	
}

