package com.elec5619.uvapp.service;

import java.io.Serializable;

import java.util.List;
import java.util.Map;



public interface GenericViewInterface extends Serializable{
	
	public Map getGenericView();
	
	public Map getNonGenericView(String header, String banner, String sidebar, String popup, String footer);
	
}
