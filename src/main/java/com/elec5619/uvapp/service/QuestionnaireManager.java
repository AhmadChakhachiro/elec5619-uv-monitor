package com.elec5619.uvapp.service;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.elec5619.uvapp.domain.User;
import com.elec5619.uvapp.domain.RiskEntity;
import com.elec5619.uvapp.service.internal.UserManager;

@Service(value="QuestionnaireInterface")
@Transactional
public class QuestionnaireManager implements QuestionnaireInterface {
	
	private static final Logger logger = LoggerFactory.getLogger(LoginManager.class);
	private UserManager userManager = new UserManager();
	
	@Autowired
	private SessionFactory sessionFactory;
	
	
	public boolean checkQuestStatus(String username) throws Exception {
		User user = this.userManager.getUser(username, this.sessionFactory);
		return user.getQuestStatus();
	}

	public Boolean updatedQuestStatus(String username) throws Exception {		
		
		try {
			if(sessionFactory == null) {
				// Stuff got broked D:
				System.out.println("Session factory broken, connection error");
				throw new Exception();
			} else {
				Session currentSession = sessionFactory.getCurrentSession();
				Query query = currentSession.createQuery("UPDATE User SET doneQuest='1' WHERE email= :candidate_email");
				query.setString("candidate_email", username);
				int result = query.executeUpdate();
				
				return true;
			}
		} catch (Exception e) {
			logger.info(e.toString());
		}
		return null;
	}
	
	private Boolean updateValue(String email, double newValue) throws Exception {
		
		try {
			if(sessionFactory == null) {
				// Stuff got broked D:
				System.out.println("Session factory broken, connection error");
				throw new Exception();
			} else {
				Session currentSession = sessionFactory.getCurrentSession();
				Query query = currentSession.createQuery("UPDATE RiskEntity SET riskValue=:value WHERE email= :candidateEmail");
				query.setParameter("candidateEmail", email);
				query.setParameter("value", newValue);
				int result = query.executeUpdate();
				
				return true;
			}
		} catch (Exception e) {
			logger.info(e.toString());
		}
		
		return null;
	}
	
	private boolean findUserRisk(String email) throws Exception {
		RiskEntity risk = null;
		
		try {
			if (sessionFactory == null) {
				// If SessionFactory failed, exit loop.
				logger.info("Session Factory Failed");
				throw new Exception();
			} else {
				
				//Query the database for matching user email.
				Session currentSession = sessionFactory.getCurrentSession();
				Query q = currentSession.createQuery("FROM RiskEntity WHERE email = :candidateEmail");
				q.setParameter("candidateEmail", email);
				List results = q.list();
				
				if (results.size() == 1) {
					// User found, returning unique RiskEntity type.
					return true;
				}	
			}
		} catch (Exception e) {
			logger.info(e.toString());
		}
		return false;
	}

	public Double getUserRisk(String email) throws Exception {
		Double risk = null;
		
		try {
			if (sessionFactory == null) {
				// If SessionFactory failed, exit loop.
				logger.info("Session Factory Failed");
				throw new Exception();
			} else {
				
				//Query the database for matching user email.
				Session currentSession = sessionFactory.getCurrentSession();
				Query q = currentSession.createQuery("SELECT riskValue FROM RiskEntity WHERE email = :candidateEmail");
				q.setParameter("candidateEmail", email);
				List results = q.list();
				
				if (results.size() == 1) {
					// User found, returning unique RiskEntity type.
					return (Double) q.list().get(0);
				}	
			}
		} catch (Exception e) {
			logger.info(e.toString());
		}
		return risk;
	}
	
	public Boolean updateRiskFactor(String email, double riskValue) throws Exception {
		
		try {
			if(sessionFactory == null) {
				// Stuff got broked again... DD:
				System.out.println("Session factory broken, connection error");
				throw new Exception();
			} else {				
				// First Check if user already exists.
				if (findUserRisk(email) == false) {					
					// No user in table with matching email, therefore create new one.
					RiskEntity risk = new RiskEntity(email, riskValue);
					this.sessionFactory.getCurrentSession().save(risk);
					return true;
				} else {					
					// User Exists so we just save user risk value.
					if (updateValue(email, riskValue) == true) {
						return true;
					}
				}				
				return false;
			}
		} catch (Exception e){
			logger.info(e.toString());
		}		
		return null;
	}

}