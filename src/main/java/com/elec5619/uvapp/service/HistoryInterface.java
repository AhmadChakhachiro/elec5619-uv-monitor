package com.elec5619.uvapp.service;

import java.io.Serializable;


public interface HistoryInterface extends Serializable{
	
	public Boolean checkIn(String email, double uv, double hours) throws Exception;

	public String[] getHistory(String email) throws Exception;

	public Long getLastCheckIn(String email) throws Exception;

	public Double getCumulativeUV(String email, boolean month) throws Exception;

	public Double getWeeklyExposure(String email) throws Exception;
	
}
