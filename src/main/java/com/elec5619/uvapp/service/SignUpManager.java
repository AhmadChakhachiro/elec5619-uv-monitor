package com.elec5619.uvapp.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import com.elec5619.uvapp.domain.User;
import com.elec5619.uvapp.service.internal.UserManager;

import org.hibernate.HibernateException;

//Database imports

import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service(value="signUpInterface")
@Transactional
public class SignUpManager implements SignUpInterface {
	
	private UserManager userManager = new UserManager();
	private static final Logger logger = LoggerFactory.getLogger(SignUpManager.class);
	private SessionFactory sessionFactory;
	
	@Autowired
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	@Override
	public Boolean signUp(String username, String password) throws Exception {
		User user = new User(username, password, false);

		try {
			if(sessionFactory==null){
				System.out.println("111");
				throw new Exception();
			}
			else
				return addUser(user);
		} catch (Exception e){
			logger.info(e.toString());
		}

		return false;
	}
	
	private Boolean addUser(User user) throws HibernateException, Exception {
		
		//User usert = this.userManager.getUser(user.getEmail());
		//logger.info(usert.getEmail());
		
		if (this.userManager.getUser(user.getEmail(), this.sessionFactory) == null){
			this.sessionFactory.getCurrentSession().save(user);
			return true;
		} 
		return false;		
	}

}

