package com.elec5619.uvapp.service;

import java.io.Serializable;



import com.elec5619.uvapp.domain.User;


public interface LoginInterface extends Serializable{
	
	public Boolean login(String username, String password) throws Exception;
}
