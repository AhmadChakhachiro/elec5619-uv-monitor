package com.elec5619.uvapp.service;

import java.io.Serializable;



import com.elec5619.uvapp.domain.User;


public interface SignUpInterface extends Serializable{
	
	public Boolean signUp(String username, String password) throws Exception;
	
	
}
