package com.elec5619.uvapp.service.internal;

import java.io.Serializable;

import org.hibernate.SessionFactory;

import com.elec5619.uvapp.domain.User;


public interface UserInterface extends Serializable{
	
	public User getUser (String email, SessionFactory sf)throws Exception;
	
}
