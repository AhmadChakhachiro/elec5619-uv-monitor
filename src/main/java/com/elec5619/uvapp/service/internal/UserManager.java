package com.elec5619.uvapp.service.internal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import com.elec5619.uvapp.domain.User;
import com.elec5619.uvapp.service.LoginManager;

import java.util.List;

import org.hibernate.Query;

//Database imports

import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service(value="userInterface")
@Transactional
public class UserManager implements UserInterface{
	
	private static final Logger logger = LoggerFactory.getLogger(UserManager.class);
	
	
	@Override
	public User getUser (String email, SessionFactory sf) throws Exception {
		User user = null;
		
		try {			// Make sure the session factory is working	
			if(sf==null){ 	// Session factory has not crashed but is void (server down?)
				logger.info("Session Factory Failed");
				throw new Exception();
			} 
			else{ //Session factory is up, running and connected
				
				// Query the Database
				Session currentSession = sf.getCurrentSession();
				Query query = currentSession.createQuery("FROM User  WHERE email = :candidate_email");
				query.setString("candidate_email", email);
				List result = query.list();
				
				// Check the result of the query
				if (result.size() == 1){ // Exactly one matching user email
					user = (User) result.get(0);
				}
			}
		} catch (Exception e){
			logger.info(e.toString());
		}
		return user;
	}
}

