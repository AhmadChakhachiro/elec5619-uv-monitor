package com.elec5619.uvapp.service;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.elec5619.uvapp.mappings.HTMLMappings;



public class GenericViewManager implements GenericViewInterface {
	
	private final String genericHeader = "";
	private final String genericBanner = HTMLMappings.HOME_BUTTON;
	private final String genericSidebar = HTMLMappings.HTML_SIDEBAR;
	private final String genericPopup = "";
	private final String genericFooter = "";
	
	
	public Map getGenericView(){
		Map<String, Object> myModel = new HashMap<String, Object>();
		
		myModel.put(HTMLMappings.MAP_HEADER, this.genericHeader);
		myModel.put(HTMLMappings.MAP_BANNER, this.genericBanner);
		myModel.put(HTMLMappings.MAP_SIDEBAR, this.genericSidebar);
		myModel.put(HTMLMappings.MAP_POPUP, this.genericPopup);
		myModel.put(HTMLMappings.MAP_FOOTER, this.genericFooter);
		
		return myModel;
		
	}
	
	public Map getNonGenericView(String header, String banner, String sidebar, String popup, String footer){
		
		Map<String, Object> myModel = getGenericView();
		
		if (header != null){
			myModel.put(HTMLMappings.MAP_HEADER, header);
		} 
		
		if (banner != null){
			myModel.put(HTMLMappings.MAP_BANNER, banner);
		}
		
		if (sidebar != null){
			myModel.put(HTMLMappings.MAP_SIDEBAR, sidebar);
		}
		
		if (popup != null){
			myModel.put(HTMLMappings.MAP_POPUP, popup);
		}
		
		if (footer != null){
			myModel.put(HTMLMappings.MAP_FOOTER, footer);
		}
		
		return myModel;
	}
	
}