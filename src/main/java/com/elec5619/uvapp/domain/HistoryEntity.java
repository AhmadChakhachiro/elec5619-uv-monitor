package com.elec5619.uvapp.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="UserData")
public class HistoryEntity implements Serializable{
	@Id
	@Column(name="dataid")
	private int id;
	
	@Column(name="email")
	private String email;
	
	@Column(name="uv")
	private double uv;
	
	@Column(name="uvTimestamp")
	private String dateTime;
        

    @Column(name="hours")
	private double hours;
	
	public HistoryEntity(){}
	
	public HistoryEntity(String email, double uv) {
		this.email = email;
		this.uv = uv;
		this.hours=0;
	}
        
       
	public HistoryEntity(String email, double uv, double hours) {
		this.email = email;
		this.uv = uv;
		this.hours=hours;
	}

	public double getUV(){
		return uv;
	}
	
	public String getTimestamp(){
		return dateTime;
	}
	
}
