package com.elec5619.uvapp.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.elec5619.uvapp.service.LoginManager;

import org.mindrot.jbcrypt.BCrypt;

@Entity
@Table(name="User")
public class User implements Serializable{
	private static final Logger logger = LoggerFactory.getLogger(User.class);

//	@Column(name="userId")
//	private int id;
	
	@Id
	@Column(name="email")
	private String email;

	@Column(name="password")
	private String password;
	
	// doneQuest is a flag needed to determine if User has completed the questionnaire.
	@Column(name="doneQuest")
	private boolean doneQuest;
	
	public User(){
		logger.info("User is working.");
	}
	
	public User(String email, String password, boolean doneQuest){
	
		String hashed = BCrypt.hashpw(password, BCrypt.gensalt());
		
		this.email = email;
		this.password = hashed;
		this.doneQuest = doneQuest;
	}
	
	public User(String email, boolean doneQuest) {
		this.email = email;
		this.doneQuest = doneQuest;
	}
	
	public boolean login(String password_candidate){
		return BCrypt.checkpw(password_candidate, this.password);
	}

	/*
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
*/
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public boolean getQuestStatus() {
		return doneQuest;
	}
	
	public void setQuestStatus(boolean status) {
		this.doneQuest = status;
	}
}
