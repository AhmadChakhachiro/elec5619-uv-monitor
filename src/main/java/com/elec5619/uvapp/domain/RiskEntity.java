package com.elec5619.uvapp.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="riskData")
public class RiskEntity implements Serializable {

	@Id
	@Column(name="email")
	private String email;
	
	@Column(name="riskValue")
	private double riskValue;
	
	public RiskEntity(){}
	
	public RiskEntity(String email, double riskValue) {
		this.email = email;
		this.riskValue = riskValue;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public double getriskValue() {
		return riskValue;
	}
	
	public void setRiskValue(double riskValue) {
		this.riskValue = riskValue;
	}
}
