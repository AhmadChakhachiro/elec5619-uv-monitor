package com.elec5619.uvapp.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.elec5619.uvapp.service.LoginManager;

import org.mindrot.jbcrypt.BCrypt;

@Entity
@Table(name="BOMData")
public class UVEntity implements Serializable{
	private static final Logger logger = LoggerFactory.getLogger(UVEntity.class);

//	@Column(name="userId")
//	private int id;
	
	@Id
	@Column(name="location")
	private String location;
	
	@Column(name="uv")
	private double uv;
	
	
	public double getUV(){
		return uv;
	}
	
}
