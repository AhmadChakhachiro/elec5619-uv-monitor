package com.elec5619.uvapp.test;

import static org.junit.Assert.*;
import javax.annotation.Resource;

import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.elec5619.uvapp.service.QuestionnaireInterface;
import com.elec5619.uvapp.service.SignUpInterface;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:spring/persistence-context",
									"classpath*:spring/servlet-context.xml", 
									"classpath*:spring/applicationContext-security.xml",
									"classpath*:spring/context.xml",
									"classpath*:spring/log4j.xml",
									"classpath*:spring/web.xml"})
public class questionnaireTest {
	static String userQuestYes = "testYes@test.com";;
	static String userQuestNo = "testNo@test.com";
	static String unknownUser = "unknown@unknown.com";
	String pass = "test";

	@Resource(name = "QuestionnaireInterface")
	private QuestionnaireInterface QuestionnaireInterface;

	@Resource(name="signUpInterface")
	private SignUpInterface signUpInterface;

	@Test
	public void testQuestStatus() throws Exception {
		
		this.signUpInterface.signUp(userQuestYes, pass);
		this.signUpInterface.signUp(userQuestNo, pass);
		
		// First update quest status of userQuestYes to flag 1 for questStatus.
		assertTrue(this.QuestionnaireInterface.updatedQuestStatus(userQuestYes));
		// Test to see if user not saved in database would be updated.
		assertNull(this.QuestionnaireInterface.updatedQuestStatus(unknownUser));
		
		// Check if QuestStatus of userQuestYes returns True.
		assertTrue(this.QuestionnaireInterface.checkQuestStatus(userQuestYes));
		// Test to see if questStatus of userQuestNo returns false.
		assertFalse(this.QuestionnaireInterface.checkQuestStatus(userQuestNo));
		// Test to see if user not in database returns anything.
		assertNull(this.QuestionnaireInterface.checkQuestStatus(unknownUser));
	}

	@Test
	public void testUserRisk() throws Exception {
		
		// Test to receive known users risk scores.
		assertNotNull(this.QuestionnaireInterface.getUserRisk(userQuestYes));
		Double value = 15.0;		
		assertEquals(this.QuestionnaireInterface.getUserRisk(userQuestYes),value);
		
		// Test if newly added user would return a new riskValue.
		assertTrue(this.QuestionnaireInterface.updateRiskFactor(userQuestNo, 8));
		value = 8.0;
		assertEquals(this.QuestionnaireInterface.getUserRisk(userQuestNo), value);
		
	}
}
