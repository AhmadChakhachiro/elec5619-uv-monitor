package com.elec5619.uvapp.test;

import static org.junit.Assert.*;
import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.elec5619.uvapp.service.LoginInterface;
import com.elec5619.uvapp.service.SignUpInterface;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:spring/persistence-context",
									"classpath*:spring/servlet-context.xml", 
									"classpath*:spring/applicationContext-security.xml",
									"classpath*:spring/context.xml",
									"classpath*:spring/log4j.xml",
									"classpath*:spring/web.xml"})
public class UserTest {
	
	String username = "signUpTest@test.com";
	String password = "test";
	
	
	@Resource(name="signUpInterface")
	private SignUpInterface signUpInterface;	
	@Resource(name = "loginInterface")
	private LoginInterface loginInterface;	

	@Test
	public void testSignUp() throws Exception {	
		
		// Returns true on newly made account.
		assertTrue(this.signUpInterface.signUp(username, password));
		
		// Should return false since account exists on DB.
		assertFalse(this.signUpInterface.signUp("archi@test.com", "test"));
	}
	
	@Test
	public void testLogIn() throws Exception {
		
		// Check if user created earlier exists on DB and returns account validation.
		assertTrue(this.loginInterface.login(username, password));
		
		// Check if invalid user returns.
		assertFalse(this.loginInterface.login("randomName@rando.com", "randoPass"));
		
		// Check if invalid name returns account validation.
		assertFalse(this.loginInterface.login(username, "incorrectPass"));
	}

}
